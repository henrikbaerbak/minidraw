/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import minidraw.standard.AbstractFigure;

import java.awt.*;

/** A figure representing a text.

   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Department of Computer Science
     Aarhus University
   
   Please visit http://www.baerbak.com/ for further information.

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at
 
       http://www.apache.org/licenses/LICENSE-2.0
 
   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

 */
public class TextFigure extends AbstractFigure {
  protected final Color color;
  private final int fontSize;
  protected Point position;
  protected String text;
  protected Font fFont;

  protected static FontMetrics metrics;
  protected Dimension extent;

  public TextFigure(String text, Point position, Color color, int fontSize) {
    this.position = position;
    this.text = text;
    this.color = color;
    this.fontSize = fontSize;
    extent = textExtent();
    fFont = new Font("Serif", Font.BOLD, fontSize);
  }

  public void setText(String newText) {
    willChange();
    text = newText;
    extent = textExtent();
    changed();
  }

  protected void basicMoveBy(int dx, int dy) {
    position.translate(dx, dy);
  }

  public Rectangle displayBox() {
    extent = textExtent();
    return new Rectangle(position.x, position.y,
            extent.width, extent.height);
  }

  public void draw(Graphics g) {
    Graphics2D g2 = (Graphics2D) g;
    g2.setFont(fFont);
    g2.setColor(color);
    g2.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
    metrics = g.getFontMetrics(fFont);
    g.drawString(text, position.x, position.y + metrics.getAscent());
  }

  protected Dimension textExtent() {
    // metrics may not have been defined yet if no drawing
    // has occurred, however the error is removed upon first
    // redrawing.
    int fWidth;
    int fHeight; 
    if ( metrics == null ) {
      fWidth = estimateWidth();
      fHeight = 20;
    } else {
      fWidth = metrics.stringWidth(text);
      fHeight = metrics.getHeight();
    }
    return new Dimension(fWidth, fHeight);
  }

  private int estimateWidth() {
    if (fontSize == 16) {
      return (int)(text.length() * 9.72);
    } else
      return (int) (text.length() * 14.5);
  }
}
