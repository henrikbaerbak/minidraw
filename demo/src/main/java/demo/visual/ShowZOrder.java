/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import minidraw.framework.DrawingEditor;
import minidraw.framework.Figure;
import minidraw.framework.Tool;
import minidraw.framework.ZOrder;
import minidraw.standard.ImageFigure;
import minidraw.standard.MiniDrawApplication;
import minidraw.standard.NullTool;
import minidraw.standard.SelectionTool;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Date;

/**
 * Demonstrate V2.0's ability to Z order figures.
 */
public class ShowZOrder {

  public static void main(String[] args) {
    DrawingEditor editor = new MiniDrawApplication(
        "(Bk, Wk, Bp, Wp), click to Z order TO BOTTOM", new ChessBackgroundFactory());
    editor.open();

    Figure blackKing = new ImageFigure("bking",
        new Point(14 + 3 * 40, 14 + 0 * 40));
    editor.drawing().add(blackKing);

    Figure whiteKing = new ImageFigure("wking",
        new Point(17 + 3 * 40, 17 + 0 * 40));
    editor.drawing().add(whiteKing);

    Figure blackPawn = new ImageFigure("bpawn",
        new Point(10 + 3 * 40, 12 + 0 * 40));
    editor.drawing().add(blackPawn);

    Figure whitePawn = new ImageFigure("wpawn",
        new Point(19 + 3 * 40, 14 + 0 * 40));
    editor.drawing().add(whitePawn);

    editor.setTool(new ZOrderChangeTool(editor));

  }

}

class ZOrderChangeTool extends NullTool {
  private final DrawingEditor editor;

  public ZOrderChangeTool(DrawingEditor editor) {
    this.editor = editor;
  }

  @Override
  public void mouseDown(MouseEvent e, int x, int y) {
    super.mouseDown(e, x, y);
    Figure f = editor.drawing().findFigure(x,y);
    if (f != null) {
      System.out.println(" --> Moving: " + editor.drawing().zOrder(f, ZOrder.TO_BOTTOM) + " To bottom.");
      ;
    }
  }
}
