/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import java.awt.*;
import java.util.Date;

import minidraw.framework.*;
import minidraw.standard.*;

/**
 * Test that figures can be added to the drawing and that the
 * selection tool works. Test that selection tool can move individual
 * figures as well as select a set of figures for composite movement.

 * Demonstrate incremental drawing, in that the WhitePawn will output
 * on StdOut when it is requested to paint itself, but only when
 * its rectangle is 'dirty'.
 * 
 */
public class ShowFigures {

  public static void main(String[] args) {
    DrawingEditor editor = new MiniDrawApplication(
        "Test: Figures appear; select tool works; inc repaint", new ChessBackgroundFactory());
    editor.open();
    Figure blackKing = new ImageFigure("bking",
        new Point(14 + 3 * 40, 14 + 0 * 40));
    editor.drawing().add(blackKing);

    Figure whiteKing = new ImageFigure("wking",
        new Point(14 + 3 * 40, 14 + 7 * 40));
    editor.drawing().add(whiteKing);

    Figure blackPawn = new ImageFigure("bpawn",
        new Point(14 + 4 * 40, 14 + 1 * 40));
    editor.drawing().add(blackPawn);

    Figure whitePawn = new TalkingImageFigure("wpawn",
        new Point(14 + 4 * 40, 14 + 6 * 40));
    editor.drawing().add(whitePawn);

    editor.setTool(new SelectionTool(editor));

  }
}

class TalkingImageFigure extends ImageFigure {

  private String name;
  
  public TalkingImageFigure(String imageName, Point position) {
    super(imageName, position);
    this.name = imageName;
  }

  public void draw(Graphics g) {
    Date d = new Date();
    System.out.println("Drawing "+name+" at "+displayBox() + " at " + d);
    super.draw(g);
  }
  
}
