/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import minidraw.framework.*;
import minidraw.standard.*;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/** Demonstrate a simple animation.
 */
public class ShowAnimation {
  public static void main(String[] args) {
    DrawingEditor editor = new MiniDrawApplication(
            "Show Animation. Click to animate boxes ...", new SimpleFactory());
    editor.open();

    java.util.List<Figure> aSet = new ArrayList<>();
    for (int i = 0; i <= 6; i++) {
      RectangleFigure rectangle =
              new RectangleFigure(new Point(20, 20 + i * 50),
                      new Point(100, 60 + i * 50));
      aSet.add(rectangle);
      editor.drawing().add(rectangle);
    }

    editor.setTool(new MoveBackAndForthTool(aSet));
  }
}

// A test stub tool.
class MoveBackAndForthTool extends NullTool {
  private List<Figure> figureList;

  public MoveBackAndForthTool(java.util.List<Figure> figureList) {
    this.figureList = figureList;
  }

  int count = 0;
  public void mouseUp(MouseEvent e, int x, int y) {
    if (count++ % 2 == 0) {
      animateToNewX(+480);
    } else {
      animateToNewX(+20);
    }
  }

  private void animateToNewX(final int newX) {
    for (Figure figure : figureList) {
      // Appease Java with final stuff...
      final Figure thisFig = figure;
      Thread animator = new Thread(new Runnable() {
        @Override
        public void run() {
          int dx = newX - thisFig.displayBox().x;

          double resolution = 150.0;
          double dxtenth = dx / resolution;
          for (int i = 0; i < resolution; i++) {
            thisFig.moveBy((int) dxtenth, 0);
            try {
              Thread.sleep(5);

            } catch (InterruptedException e) {
              e.printStackTrace();
            }
          }

          // Ensure hitting the target
          dx = newX - thisFig.displayBox().x;
          thisFig.moveBy(dx, 0);
        }
      });
      animator.start();
    }
  }
}

