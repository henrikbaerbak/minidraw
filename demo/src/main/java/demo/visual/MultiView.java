/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import java.awt.Point;

import javax.swing.*;

import minidraw.framework.*;
import minidraw.standard.*;

/**
 * Demonstrate multiple views.
 * 
 */
public class MultiView {

  public static void main(String[] args) {
    Factory f = new ChessBackgroundFactory();
    DrawingEditor editor = new MiniDrawApplication("Multi view", f);
    editor.open();

    Figure blackKing = new ImageFigure("bking",
        new Point(14 + 3 * 40, 14 + 0 * 40));
    editor.drawing().add(blackKing);

    Figure whiteKing = new ImageFigure("wking",
        new Point(14 + 3 * 40, 14 + 7 * 40));
    editor.drawing().add(whiteKing);

    editor.setTool(new SelectionTool(editor));

    // create second view
    JFrame newWindow = new JFrame("Extra View");
    JFrame.setDefaultLookAndFeelDecorated(true);
    newWindow.setLocation(620, 20);
    newWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    DrawingView extraView = f.createDrawingView(editor);
    JPanel panel = (JPanel) extraView;
    newWindow.getContentPane().add(panel);
    newWindow.pack();
    newWindow.setVisible(true);
  }
}
