/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import minidraw.framework.*;
import minidraw.standard.*;

import java.awt.*;
import java.awt.event.MouseEvent;

/** Demonstrate that changing an internal figure inside a
 * composite figure, will bubble up and make the whole
 * composite changed (to ensure firing the observer chain).
 */
public class ShowCompositeRedraw {
  public static void main(String[] args) {
    DrawingEditor editor = new MiniDrawApplication(
            "Show Composite internal figure redraw by clicking ...", new SimpleFactory());
    editor.open();

    TextFigure textFigure = new TextFigure("Uno",
            new Point(280,190), Color.YELLOW, 16);
    RectangleFigure rectFigure = new RectangleFigure( new Point(200,150),
            new Point(400, 250));

    CompositeFigure composite = new CompositeFigure() {
      @Override
      public void draw(Graphics g) {
        super.draw(g);
        // Add bounding box
        g.setColor(Color.ORANGE);
        g.drawRect(displayBox().x, displayBox().y,
                displayBox().width - 1, displayBox().height - 1);
      }
    };

    // Make the composite be defined by a rectangle and a text
    composite.add(rectFigure);
    composite.add(textFigure);

    editor.drawing().add(composite);

    editor.setTool(new ChangeStuffTool(rectFigure, textFigure));
  }

}

// A test stub tool.
class ChangeStuffTool extends NullTool {
  private RectangleFigure rectFigure;
  private TextFigure textFigure;
  private String[] theTexts = { "Uno", "Dos", "Tres", "Cuatro", "Cinco",
          "Seis", "Siete"};

  public ChangeStuffTool(RectangleFigure rectFigure, TextFigure tf) {
    this.rectFigure = rectFigure;
    textFigure = tf;
  }

  int count = 0;
  public void mouseUp(MouseEvent e, int x, int y) {
    count++;
    textFigure.setText( theTexts[count % 7] );
    if (count % 3 == 0) {
      rectFigure.setColor(new Color(128, 128, 0));
    } else
      rectFigure.setColor(new Color(0, 128, 0));
  }
}

