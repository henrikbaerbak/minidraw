/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import minidraw.framework.DrawingEditor;
import minidraw.framework.Figure;
import minidraw.framework.Tool;
import minidraw.standard.CompositeFigure;
import minidraw.standard.ImageFigure;
import minidraw.standard.MiniDrawApplication;
import minidraw.standard.NullTool;

import java.awt.*;
import java.awt.event.MouseEvent;

public class ShowCompositeSubfigureChange {
  public static void main(String[] args) {
    DrawingEditor editor = new MiniDrawApplication(
            "Show Composite internal figure changing by clicking ...", new SimpleFactory());
    editor.open();

    Figure frame = new ImageFigure("frame", new Point());
    Figure emblem = new ImageFigure("Green-Salad", new Point(20, 18));

    CompositeFigure composite = new CompositeFigure();

    // Make the composite by overlaying the frame on the emblem
    composite.add(emblem);
    composite.add(frame, "frame");

    composite.moveBy(20,20);

    editor.drawing().add(composite);

    editor.setTool(new ModifyFrameTool(composite));
  }

  private static class ModifyFrameTool extends NullTool {
    private final CompositeFigure composite;

    public ModifyFrameTool(CompositeFigure composite) {
      this.composite = composite;
    }

    int clickCount = 0;
    @Override
    public void mouseUp(MouseEvent e, int x, int y) {
      super.mouseUp(e, x, y);
      clickCount++;

      // Change the frame to another
      Figure f = composite.getByTag("frame");
      ImageFigure imf = (ImageFigure) f;
      if (clickCount % 2 == 1)
        imf.set("taunt-frame-sd");
      else
        imf.set("frame");
    }
  }
}
