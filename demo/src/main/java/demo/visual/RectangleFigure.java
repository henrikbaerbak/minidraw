/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import minidraw.standard.AbstractFigure;

import java.awt.*;

public class RectangleFigure extends AbstractFigure {

  private Color myColor;
  private Rectangle fDisplayBox;

  public RectangleFigure(Point origin, Point corner) {
    basicResize(origin, corner);
    myColor = new Color(0, 128, 0);
  }

  public RectangleFigure(Point origin) {
    basicResize(origin, origin);
    myColor = new Color(0, 128, 0);
  }

  protected void basicResize(Point origin, Point corner) {
    fDisplayBox = new Rectangle(origin);
    fDisplayBox.add(corner);
  }

  @Override
  public Rectangle displayBox() {
    return new Rectangle(fDisplayBox.x, fDisplayBox.y, fDisplayBox.width,
            fDisplayBox.height);
  }

  @Override
  protected void basicMoveBy(int x, int y) {
    fDisplayBox.translate(x, y);
  }

  protected void resize(Point origin, Point corner) {
    willChange();
    basicResize(origin, corner);
    changed();
  }

  @Override
  public void draw(Graphics g) {
    Rectangle r = displayBox();
    g.setColor(myColor);
    g.fillRect(r.x, r.y, r.width, r.height);
    g.setColor(Color.BLACK);
    g.drawRect(r.x, r.y, r.width - 1, r.height - 1);
  }

  public void setColor(Color myColor) {
    willChange();
    this.myColor = myColor;
    changed();
  }
}
