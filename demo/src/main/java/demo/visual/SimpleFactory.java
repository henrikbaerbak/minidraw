/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import minidraw.framework.Drawing;
import minidraw.framework.DrawingEditor;
import minidraw.framework.DrawingView;
import minidraw.framework.Factory;
import minidraw.standard.CompositionalDrawing;
import minidraw.standard.StandardDrawingView;

import javax.swing.*;
import java.awt.*;

public class SimpleFactory implements Factory {
  @Override
  public DrawingView createDrawingView(DrawingEditor editor) {
    return new StandardDrawingView(editor, new Dimension(600, 400)) {

      @Override
      public void drawBackground(Graphics g) {
        g.setColor(Color.darkGray);
        g.fillRect(0, 0, getBounds().width, getBounds().height);
      }
    };
  }

  @Override
  public Drawing createDrawing(DrawingEditor editor) {
    return new CompositionalDrawing();
  }

  @Override
  public JTextField createStatusField(DrawingEditor editor) {
    return null;
  }
}


