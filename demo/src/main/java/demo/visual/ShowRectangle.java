/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.visual;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

import minidraw.framework.*;
import minidraw.standard.*;

/**
 * Demonstrate MiniDraw ability to: A) Handle multiple windows B) Define new
 * Figure instances (green rectangles) C) Define a new tool to create rectangles
 * D) Switching tools at run-time E) Showing text in the status window
 * and F) Z Ordering of figures (moving up/down in stack)
 * 
 */
public class ShowRectangle {

  public static void main(String[] args) {
    Factory f = new EmptyCanvasFactory();
    DrawingEditor window = new MiniDrawApplication(
        "Create, move, and ZOrder rectangles - use the mouse", f);
    Tool rectangleDrawTool = new RectangleTool(window),
        selectionTool = new SelectionTool(window),
            toBottomTool = new ZOrderTool(window);
    addToolSelectMenusToWindow(window, rectangleDrawTool,
            selectionTool, toBottomTool);
    window.open();

    window.setTool(new RectangleTool(window));

    // create second view
    JFrame newWindow = new JFrame("Extra View");
    newWindow.setLocation(620, 20);
    newWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    DrawingView extraView = f.createDrawingView(window);
    JPanel panel = (JPanel) extraView;
    newWindow.getContentPane().add(panel);
    newWindow.pack();
    newWindow.setVisible(true);
  }

  public static void addToolSelectMenusToWindow(final DrawingEditor window,
      final Tool rectangleTool, final Tool selectionTool, final Tool toBottomTool) {
    // Here I use the knowledge that 'window' is a MiniDrawApplication
    JFrame frame = (JFrame) window;

    JMenuBar menuBar;
    JMenu menu;
    JMenuItem menuItem;

    menuBar = new JMenuBar();
    menu = new JMenu("Tool");
    menuBar.add(menu);

    menuItem = new JMenuItem("Rectangle");
    menuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        window.setTool(rectangleTool);
        window.showStatus("Rectangle tool active.");
      }
    });
    menu.add(menuItem);

    menuItem = new JMenuItem("Selection");
    menuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        window.setTool(selectionTool);
        window.showStatus("Selection tool active.");
      }
    });
    menu.add(menuItem);

    menuItem = new JMenuItem("ZOrder");
    menuItem.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        window.setTool(toBottomTool);
        window.showStatus("Z-Order Tool. Shift=Forward, Ctrl=Backward, Shift+Ctrl=ToTop, None=ToBack");
      }
    });
    menu.add(menuItem);
    frame.setJMenuBar(menuBar);
  }

}

class EmptyCanvasFactory implements Factory {

  @Override
  public DrawingView createDrawingView(DrawingEditor editor) {
    return new StandardDrawingView(editor, new Dimension(400, 200));
  }

  @Override
  public Drawing createDrawing(DrawingEditor editor) {
    return new CompositionalDrawing();
  }

  @Override
  public JTextField createStatusField(DrawingEditor editor) {
    return new JTextField();
  }
}

/** A tool to create rectangles */
class RectangleTool extends AbstractTool {
  private RectangleFigure f;

  public RectangleTool(DrawingEditor editor) {
    super(editor);
    f = null;
  }

  @Override
  public void mouseDown(MouseEvent e, int x, int y) {
    super.mouseDown(e, x, y);
    f = new RectangleFigure(new Point(x, y));
    editor.drawing().add(f);
  }

  @Override
  public void mouseDrag(MouseEvent e, int x, int y) {
    f.resize(new Point(fAnchorX, fAnchorY), new Point(x, y));
  }

  @Override
  public void mouseUp(MouseEvent e, int x, int y) {
    if (f.displayBox().isEmpty()) {
      editor.drawing().remove(f);
    }
    f = null;
  }
}

class ZOrderTool extends NullTool {
  private final DrawingEditor editor;

  public ZOrderTool(DrawingEditor window) {
    this.editor = window;
  }

  @Override
  public void mouseUp(MouseEvent e, int x, int y) {
    super.mouseUp(e, x, y);
    Figure f = editor.drawing().findFigure(x,y);
    if (f != null) {
      if (e.isShiftDown() && e.isControlDown()) {
        editor.showStatus("Shift+Ctrl = Z Order TOP");
        editor.drawing().zOrder(f, ZOrder.TO_TOP);
      } else if (e.isControlDown()) {
        editor.showStatus("Ctrl = Z Order BACKWARD");
        editor.drawing().zOrder(f, ZOrder.BACKWARD);
      } else if (e.isShiftDown()) {
        editor.showStatus("Shift = Z Order FORWARD");
        editor.drawing().zOrder(f, ZOrder.FORWARD);
      } else {
        editor.showStatus("None = Z Order BOTTOM");
        editor.drawing().zOrder(f, ZOrder.TO_BOTTOM);
      }
    }
  }
}
