/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package demo.breakthrough;

import java.awt.Point;

import minidraw.boardgame.PositioningStrategy;

/**
 * The strategy for positioning chess pawns properly in the center of a square
 * on the chess board. 
 *  */
public class ChessBoardPositioningStrategy
    implements PositioningStrategy<Position> {

  @Override
  public Point calculateFigureCoordinatesIndexedForLocation(Position location,
      int index) {
    // ignore index, there is only one piece at a time on any square
    return new Point(
        location.c * Constants.SQUARE_SIZE + Constants.SQUARE_OFFSET_X,
        location.r * Constants.SQUARE_SIZE + Constants.SQUARE_OFFSET_Y);
  }

  @Override
  public Point calculateFigureCoordinatesForProps(String keyOfProp) {
    // no props are used.
    return null;
  }

}
