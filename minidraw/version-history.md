Release History of MiniDraw
===========================

 * Pre 1.0: Many many releases for various course instances.

 * 1.0: Version of MiniDraw at the time of book submission to CRC Press.

 * 1.1: Defect in boardgame hotspots fixed. Move from a location to
   the same location would not readjust the figure on the graphical
   location.

 * 1.2: Reduced the number of hotspots associated with the positioning
   strategy in the boardgame extensions.
  
 * 1.3: Changes introduced to handle 'props' in the BoardGame
   extension of MiniDraw.

 * 1.4: Some defensive programming techniques introduced in BoardGame
   extension.
 
 * 1.5: Fixed bug concerning movement of props in BoardGame. Minor documentation
   changes.
  
 * 1.6: Fixed bug in BoardGame's BoardActionTool concerning actions
   performed on props that return false.
  
 * 1.7: Several changes made in the notification and repainting
   sequence as some defects were discovered (which actually are
   embedded in the original JHotDraw code as well.) See comment in the
   source code below.
  
 * 1.8: Added hook method in MiniDrawApplication to override windows close
   operation.
  
 * 1.9: Updated ImageManager so a) it also looks for image files in a
   folder /resources to make it work together with gradle more easily,
   b) also will load PNG and JPG files (having '.png' or '.jpg'
   suffixes).
 
 * 1.10: Updated ImageManager so a) actually works in a gradle
   context: put all your wanted images in
   /src/main/resources/minidraw/ subfolder in the default gradle
   project structure.
 
 * 1.11: Redefined the image folder for Gradle builds to be
  /src/main/resources/minidraw-images to avoid conflicts in resource
  loading
  
 * 1.12: Moved MiniDraws to open source on BitBucket, created
   deployment structure using gradle, uploaded to JCenter.

 * 1.13: Bugfix by Frederik Madsen, allow minidraw to load images from
   folders with a space in the filename.
 
 * 1.14: Fixed unnoticed bug in incremental redrawing in
   StandardDrawingView
 
 * 1.15: No code change, but migrated hosting to MavenCentral.
 
 * 2.0: Major update, with incompatible API changes, including
     * figureRequestUpdate. figureRequestRemove, figureRemoved are
       removed in FigureChangeListener, as requests semantics is
       undefined, and has not been used in MiniDraw applications
     * Refactored StandardDrawing to use a compositional design,
       which lead to introducing 'FigureCollection'.
     * Introducing 'FigureCollection' interface and Standard
       implementation - the role of a collection of figures,
       replacing the unholy use of CompositeFigure as superclass
       to StandardDrawing.
     * Drawing now extends FigureCollection.
     * Introducing a ForwardingFigureChangeHandler which implements
       the default Drawing behaviour, namely to convert any
       incoming change events from figures to a corresponding
       DrawingChange event.
     * Introducing initial Z-ordering in Drawing, with the methods
       'zOrder' introduced.
       
 * 2.1: Internal update, now Drawing implements Iterable<Figure>.
        lock() and unlock() deprecated, in favor of snapshot
        safe iterator. 
        
 * 2.2: Fixed serious concurrency bug, as dirty-rect management
        was not thread safe. Introduced a Reentrant read/write
        lock in StandardDrawingView to solve this issue.
        Added 'moveTo()' method in Figure.
        
 * 2.3: Updated ImageManager to have a lazy mode to support
        reading image data from within a jar file.

 * 3.0: Refactored CompositeFigure to delegate to FigureCollection.
        Removed GroupFigure and thus breaking API compatability.
        
 * 3.0.1: Further updates as some concurrency issues with dirty
        rectangle management persists. Added module-info, updated
        required java version to JDK 11.
 
 * 4.0: Added ability to get figures in a collection (and thereby
        in a CompositeFigure), optionally by a 'tag'. Thus, sub-
        figures may be individually altered in a composite.
        Removed module-info file as it posed more liabilities than 
        benefits. Fixed a mixup of
        read and write lock usage in StdDrawingView.
