
/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.framework;

/**
 * Drawing is the model role of the MVC pattern, a container of Figure instances
 * in MiniDraw.
 *
 * Responsibilities: A) Maintain the set of figures (add,remove). B) Maintain a
 * selection of a subset of these C) Serve as Subject in observer pattern and
 * notify DrawingListeners when changes to any figure happens. D) Serve as
 * Observer in the observer pattern as it observes all changes in its associated
 * Figures.
 *
 */
public interface Drawing extends FigureCollection, SelectionHandler,
        FigureChangeListener, DrawingChangeListenerHandler {

  /**
   * Request update: Emit a "repaint" event to all associated listeners on this
   * drawing. Normally not required to be called, as figure modifications will
   * trigger repainting through their respective listener chains.
   */
  void requestUpdate();
}
