/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.framework;

import java.awt.*;

/**
 * This interface defines the role of a Figure in a MiniDraw drawing. Figure
 * instances acts as
 * 
 * Responsibilities: A) Represent a Figure in the model. B) Draw itself in a
 * Graphics context. C) Allow manipulations like moving. D) Has the Subject role
 * in the observer pattern as a Figure broadcasts FigureChangeEvents whenever it
 * changes.
 */

public interface Figure {

  /**
   * Draws the figure.
   *
   * @param g
   *          the Graphics to draw into
   */
  void draw(Graphics g);

  /**
   * Return the display box of this figure. The display box is the smallest
   * rectangle that completely contains the figure.
   *
   * @return the display box.
   */
  Rectangle displayBox();

  /**
   * Move the figure by a delta (dx, dy) offset from its present position.
   *
   * @param dx
   *          amount to move in x
   * @param dy
   *          amount to move in y
   */
  void moveBy(int dx, int dy);

  /**
   * Move the figure to a position (x, y)  from its present position.
   *
   * @param x
   *          new x coordinate
   * @param y
   *          new y coordinate
   */
  void moveTo(int x, int y);

  /**
   * Invalidates the figure. This method informs the listeners that the figure's
   * current display box is invalid and should be redrawn.
   */
  void invalidate();

  /**
   * Informs that a figure has changed its display box. This method also
   * triggers an update call for its registered observers.
   */
  void changed();

  /**
   * Adds a listener for this figure.
   * 
   * @param l
   *          the listener to associate with this figure
   */
  void addFigureChangeListener(FigureChangeListener l);

  /**
   * Removes a listener for this figure.
   * 
   * @param l
   *          the listener to remove this figure
   */
  void removeFigureChangeListener(FigureChangeListener l);
}
