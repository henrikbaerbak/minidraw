/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.framework;

import java.util.EventListener;

/**
 * DrawingChangeListener defines the observer role of an object listening to
 * DrawingChangeEvents from a Drawing.
 */
public interface DrawingChangeListener extends EventListener {

  /**
   * Called when a drawing has areas that needs to be redrawn,
   * that is, add the dirty rectangle but do not redraw
   * 
   * @param e
   *          the event containing information about the change
   *          in particular the dirty rectangle (the clip)
   */
  public void drawingInvalidated(DrawingChangeEvent e);

  /**
   * Called when the drawing wants to be refreshed,
   * that is redraw everything within the dirty
   * rectangle
   * 
   * @param e
   *          the event containing information about the change
   */
  public void drawingRequestUpdate(DrawingChangeEvent e);
}
