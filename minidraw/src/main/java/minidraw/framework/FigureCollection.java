/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.framework;

import java.util.Iterator;
import java.util.concurrent.locks.Lock;

/** The role of a collection of figures, basically
 * a composite pattern. Since v 2.0.
 *
 * Any implementation must ensure to be thread safe, so
 * mutation/access is considered a shared resource.
 *
 * v4. Added ability to get individual subfigures either
 * by index or by tag, that can be added at 'add()' time,
 * via the add(f, tag) method.
 */

public interface FigureCollection extends Iterable<Figure> {
  /**
   * Adds a figure and sets its container to refer to this drawing. If you have
   * several threads that may call add, scope it by the lock/unlock methods.
   * The Drawing role will render figures in the order they are inserted,
   * so if they overlap the LAST added figure will appear on top. Use
   * zOrder method to change ordering.
   *
   * @param figure
   *          the figure to add
   * @return the figure that was inserted.
   */
  Figure add(Figure figure);

  /** Add a figure, and associate it with a tag which
   * allows retrieval and modification by that tag.
   * @param figure the figure to add
   * @param tagName the tag to add to the figure
   * @return the figure that was inserted
   */
  Figure add(Figure figure, String tagName);

  /**
   * Removes a figure. If you have several threads that may call add, scope it
   * by the lock/unlock methods.
   *
   * @param figure
   *          the figure to remove
   * @return the figure removed
   */
  Figure remove(Figure figure);

  /**
   * Return an iterator over drawing's contents.
   *
   * @return an iterator over all figures in this drawing.
   */
  Iterator<Figure> iterator();

  /**
   * Return the figure at given index, allowing
   * modifying subfigures independently.
   * @param indexOfFigure
   * @return the figure or null if outside boundaries
   */
  Figure get(int indexOfFigure);

  /**
   * Return the figure with the given tag
   * @param tagName
   * @return the figure or null if no such tagged figure
   */
  Figure getByTag(String tagName);

  /**
   * Find and return the figure covering position (x,y).
   *
   * @param x
   *          X coordinate
   * @param y
   *          Y coordinate
   * @return the figure at position (x,y) or null if there is not any there.
   */
  Figure findFigure(int x, int y);

  /** Request reordering of the figure's Z-order, i.e. if several
   * figures overlap, move it up/down or to top/bottom of that
   * graphical 'stack'.
   * @param figure the figure to change Z order for
   * @param order the new z order of the figure (bottom, top, ...)
   * @return the Figure affected
   */
  Figure zOrder(Figure figure, ZOrder order);

  /** Retrieve a reentrant read lock for the collection of figures
   *
   * @return the reentrant read lock.
   */
  Lock readLock();

  /** Retrieve a reentrant write lock for the collection of figures
   *
   * @return the reentrant write ad lock.
   */
  Lock writeLock();
}
