/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.framework;

import java.util.EventListener;

/**
 * The Observer role, a listener interested in Figure changes.
 *
 */
public interface FigureChangeListener extends EventListener {

  /**
   * Sent when a figure is invalid, e.g. is about to change
   * image, size, position, etc.
   * 
   * @param e
   *          the event containing information about the change
   */
  public void figureInvalidated(FigureChangeEvent e);

  /**
   * Sent when a figure changed, e.g has found its final
   * image, size, position, etc.
   * 
   * @param e
   *          the event containing information about the change
   */
  public void figureChanged(FigureChangeEvent e);
}
