/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.framework;

import java.awt.*;

/**
 * The DrawingChangeListenerHandler defines a role for an object that maintains
 * the set of DrawingChangeListener's used by a Drawing.
 * 
 * Responsibility A) Maintain the list of DrawingListeners
 * 
 */
public interface DrawingChangeListenerHandler {
  /**
   * Adds a listener for this drawing.
   *
   * @param listener the listener to add to this handler
   */
  void addDrawingChangeListener(DrawingChangeListener listener);

  /**
   * Removes a listener from this drawing.
   *
   * @param listener the listener to remove from this handler
   */
  void removeDrawingChangeListener(DrawingChangeListener listener);
}
