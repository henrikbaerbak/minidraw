/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.boardgame;

/**
 * A null object implementation of Command. All executions does nothing and
 * return true. 
 */
public class NullCommand implements Command {
  @Override
  public boolean execute() {
    return true;
  }

  @Override
  public void setFromCoordinates(int fromX, int fromY) {
  }

  @Override
  public void setToCoordinates(int toX, int toY) {
  }
}
