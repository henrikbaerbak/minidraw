/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard;

import java.awt.*;
import java.awt.event.*;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import javax.swing.JPanel;

import minidraw.framework.*;

/**
 * Standard implementation of the DrawingView role in MiniDraw, just giving an
 * empty view of the specified size.
 * 
 * Some code is copied from JHotDraw 5.1
 */
public class StandardDrawingView extends JPanel
    implements DrawingView, MouseListener, MouseMotionListener, KeyListener {

  private static final long serialVersionUID = 3722170409196832977L;

  /**
   * the object server providing yellow page access to all relevant parts of the
   * editor
   */
  protected DrawingEditor editor;

  /**
   * The dirty rectangle of this view. It basically accumulates all rectangles
   * that have seen some change since the last repaint via the rectangles
   * provided over the DrawingChangeListener's drawingInvalidated and
   * drawingRequestUpdate methods. To support concurrent modifications and
   * updates, any access must be guarded with read and write locks.
   */
  protected Rectangle dirtyRectangle;

  /** All read/write access to the dirtyRectangle must be
   * guarded by multiple reader/single writer access. */
  protected ReadWriteLock dirtyRectangleLock = new ReentrantReadWriteLock();
  protected Lock dirtyRectangleReadLock = dirtyRectangleLock.readLock();
  protected Lock dirtyRectangleWriteLock = dirtyRectangleLock.writeLock();

  /** Create a drawing view associated with the given editor
   * @param editor the editor this view is associated with
   */
  public StandardDrawingView(DrawingEditor editor) {
    this(editor, new Dimension(600, 400));
  }

  /** Create a drawing view of a given size.
   * 
   * @param editor the editor this view is associated with
   * @param size the size of the windows
   */
  public StandardDrawingView(DrawingEditor editor, Dimension size) {
    this.editor = editor;
    addMouseListener(this);
    addMouseMotionListener(this);
    addKeyListener(this);

    dirtyRectangle = null;

    // register this view as listener on change events from the
    // model
    if (editor.drawing() == null) {
      throw new RuntimeException("StandardDrawingView: Drawing undefined!");
    }
    editor.drawing().addDrawingChangeListener(this);
    setSize(size);
  }

  /** Return the associated editor
   * 
   * @return the editor
   */
  public DrawingEditor editor() {
    return editor;
  }

  @Override
  public void checkDamage() {
    editor().drawing().requestUpdate();
  }

  private void repairDamage() {
    // Queue a repaint event for the dirty rectangle
    // into the AWT/Swing rendering pipeline.
    // This will eventually trigger a call
    // to 'paint(g)' with g.getClipBounds set to
    // the dirty rectangle

    // Ensure no concurrent updates of dirty-rect is occurring
    dirtyRectangleReadLock.lock();
    try {
      if (dirtyRectangle != null) {
        repaint(dirtyRectangle.x, dirtyRectangle.y,
                dirtyRectangle.width, dirtyRectangle.height);
        dirtyRectangle = null;
      }
    } finally {
      dirtyRectangleReadLock.unlock();
    }
  }

  @Override
  public void paint(Graphics g) {
    // Called by AWT/Swing rendering pipeline
    // when a redraw is required; the
    // g.getClipBounds() contains the
    // region to redraw
    drawAll(g);
  }

  @Override
  public void drawAll(Graphics g) {
    drawBackground(g);
    drawDrawing(g);
    drawSelectionHighlight(g);
    drawOverlay(g);
  }

  @Override
  public void drawDrawing(Graphics g) {
    Drawing drawing = editor.drawing();
    // The repairDamage will "transfer" the dirtyRectangle
    // to the graphics context via the 'repaint(x,y,w,h)' call
    // and this method receives it via the graphic's clip bounds.
    Rectangle clip = g.getClipBounds();
    // Inhibit concurrent updates of the drawing and the
    // dirty rectangles while rendering the contents of
    // drawing's collection of figures...
    drawing.readLock().lock();
    dirtyRectangleReadLock.lock();
    try {
      for (Figure f : drawing) {
        // Incremental redrawing, avoid drawing figures not in
        // clipping region
        if (f.displayBox().intersects(clip))
          f.draw(g);
      }
    } finally {
      drawing.readLock().unlock();
      dirtyRectangleReadLock.unlock();
    }
  }

  @Override
  public void drawBackground(Graphics g) {
    g.setColor(Color.LIGHT_GRAY);
    g.fillRect(0, 0, getBounds().width, getBounds().height);
  }

  @Override
  public void drawSelectionHighlight(Graphics g) {
    List<Figure> l = editor().drawing().selection();
    // TODO: Implement an variability point to vary this behavior
    if (l.size() > 1) {
      g.setColor(Color.red);
      Rectangle r;
      for (Figure f : l) {
        r = f.displayBox();
        g.drawRect(r.x, r.y, r.width - 1, r.height - 1);
      }
    }
  }

  @Override
  public void drawOverlay(Graphics g) {
    // Default to NoOp
  }

  @Override
  public Dimension getPreferredSize() {
    return getSize();
  }

  @Override
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }

  /**
   * Constrains a point to the current grid.
   * 
   * @param p
   *          the point which may be constrained to be inside our view port
   * @return the constrained point
   */
  protected Point constrainPoint(Point p) {
    // constrain to view size
    Dimension size = getSize();
    // p.x = Math.min(size.width, Math.max(1, p.x));
    // p.y = Math.min(size.height, Math.max(1, p.y));
    p.x = range(1, size.width, p.x);
    p.y = range(1, size.height, p.y);

    return p;
  }

  /**
   * Constrains a value to the given range.
   * 
   * @param min
   *          minimum value
   * @param max
   *          maximum value
   * @param value
   *          value to constrain
   * 
   * @return the constrained value
   */
  private static int range(int min, int max, int value) {
    if (value < min) {
      value = min;
    }
    if (value > max) {
      value = max;
    }
    return value;
  }

  // === Mouse event handling
  protected Point fLastClick;

  /**
   * Handles mouse down events. The event is delegated to the currently active
   * tool.
   * 
   * @param e
   *          the mouse event to handle
   */
  @Override
  public void mousePressed(MouseEvent e) {
    requestFocus();
    Point p = constrainPoint(new Point(e.getX(), e.getY()));
    fLastClick = new Point(e.getX(), e.getY());
    editor.tool().mouseDown(e, p.x, p.y);
  }

  /**
   * Handles mouse drag events. The event is delegated to the currently active
   * tool.
   * 
   * @param e
   *          the mouse event to handle
   */
  @Override
  public void mouseDragged(MouseEvent e) {
    Point p = constrainPoint(new Point(e.getX(), e.getY()));
    editor.tool().mouseDrag(e, p.x, p.y);
  }

  /**
   * Handles mouse move events. The event is delegated to the currently active
   * tool.
   * 
   * @param e
   *          the mouse event to handle
   */
  @Override
  public void mouseMoved(MouseEvent e) {
    editor.tool().mouseMove(e, e.getX(), e.getY());
  }

  /**
   * Handles mouse up events. The event is delegated to the currently active
   * tool.
   * 
   * @param e
   *          the mouse event to handle
   */
  @Override
  public void mouseReleased(MouseEvent e) {
    Point p = constrainPoint(new Point(e.getX(), e.getY()));
    editor.tool().mouseUp(e, p.x, p.y);
  }

  // These listener methods are not interesting.
  @Override
  public void mouseEntered(MouseEvent e) {
  }

  @Override
  public void mouseExited(MouseEvent e) {
  }

  @Override
  public void mouseClicked(MouseEvent e) {
  }

  @Override
  public void keyTyped(KeyEvent e) {
  }

  @Override
  public void keyReleased(KeyEvent e) {
  }

  @Override
  public void keyPressed(KeyEvent e) {
    editor.tool().keyDown(e, e.getKeyCode());
  }

  @Override
  public void drawingInvalidated(DrawingChangeEvent e) {
    // lock the dirty-rect while updating
    dirtyRectangleWriteLock.lock();
    try {
      Rectangle r = e.getInvalidatedRectangle();
      if (dirtyRectangle == null) {
        dirtyRectangle = r;
      } else {
        dirtyRectangle.add(r);
      }
    } finally {
      dirtyRectangleWriteLock.unlock();
    }
  }

  @Override
  public void drawingRequestUpdate(DrawingChangeEvent e) {
    // Initiate a redraw
    repairDamage();
  }
}
