/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard;

import minidraw.framework.*;
import minidraw.standard.handlers.ForwardingFigureChangeHandler;
import minidraw.standard.handlers.StandardDrawingChangeListenerHandler;
import minidraw.standard.handlers.StandardSelectionHandler;

import java.util.Iterator;
import java.util.List;
import java.util.concurrent.locks.Lock;

/** MiniDraw v 2.0 reimplementation of StandardDrawing, removing
 * previous use of CompositeFigure as superclass for maintaining
 * actual figure instances, replaced by a compositional approach,
 * using an instance of a FigureCollection, and a StandardFigureChangeHandler.
 */
public class CompositionalDrawing implements Drawing {

  /** list of all figures currently selected */
  protected SelectionHandler selectionHandler;

  /**
   * use a StandardDrawingChangeListenerHandler to handle all observer pattern
   * subject role behaviour
   */
  protected StandardDrawingChangeListenerHandler listenerHandler;
  protected FigureCollection figureCollection;
  protected FigureChangeListener figureChangeListener;

  public CompositionalDrawing() {
    selectionHandler = new StandardSelectionHandler();
    listenerHandler = new StandardDrawingChangeListenerHandler();
    figureChangeListener = new ForwardingFigureChangeHandler(this, listenerHandler);
    figureCollection = new StandardFigureCollection(figureChangeListener);
  }

  // === Delegation methods for the DrawingChangeListeners

  /**
   * Adds a listener for this drawing.
   */
  @Override
  public void addDrawingChangeListener(DrawingChangeListener listener) {
    listenerHandler.addDrawingChangeListener(listener);
  }

  /**
   * Removes a listener from this drawing.
   */
  @Override
  public void removeDrawingChangeListener(DrawingChangeListener listener) {
    listenerHandler.removeDrawingChangeListener(listener);
  }

  /**
   * Invalidates a rectangle and merges it with the existing damaged area.
   *
   * @see FigureChangeListener
   */
  @Override
  public void figureInvalidated(FigureChangeEvent e) {
    listenerHandler.fireDrawingInvalidated(this, e.getInvalidatedRectangle());
  }

  @Override
  public void figureChanged(FigureChangeEvent e) {
    listenerHandler.fireDrawingRequestUpdate(this);
  }

  // === Delegate to the figure collection
  @Override
  public Figure add(Figure figure) {
    return figureCollection.add(figure);
  }

  @Override
  public Figure add(Figure figure, String tagName) {
    return figureCollection.add(figure, tagName);
  }

  @Override
  public Figure remove(Figure figure) {
    return figureCollection.remove(figure);
  }

  @Override
  public Figure get(int indexOfFigure) {
    return figureCollection.get(indexOfFigure);
  }

  @Override
  public Figure getByTag(String tagName) {
    return figureCollection.getByTag(tagName);
  }

  @Override
  public Iterator<Figure> iterator() {
    return figureCollection.iterator();
  }

  @Override
  public Figure findFigure(int x, int y) {
    return figureCollection.findFigure(x, y);
  }

  @Override
  public void requestUpdate() {
    listenerHandler.fireDrawingRequestUpdate(this);
  }

  @Override
  public Figure zOrder(Figure f, ZOrder order) {
    return figureCollection.zOrder(f, order);
  }

  @Override
  public Lock readLock() {
    return figureCollection.readLock();
  }

  @Override
  public Lock writeLock() {
    return figureCollection.writeLock();
  }

  // === Delegation methods for the Selection handling

  /**
   * Get a list of all selected figures
   */
  @Override
  public List<Figure> selection() {
    return selectionHandler.selection();
  }

  /**
   * Adds a figure to the current selection.
   */
  @Override
  public void addToSelection(Figure figure) {
    selectionHandler.addToSelection(figure);
  }

  /**
   * Removes a figure from the selection.
   */
  @Override
  public void removeFromSelection(Figure figure) {
    selectionHandler.removeFromSelection(figure);
  }

  /**
   * If a figure isn't selected it is added to the selection. Otherwise it is
   * removed from the selection.
   */
  @Override
  public void toggleSelection(Figure figure) {
    selectionHandler.toggleSelection(figure);
  }

  /**
   * Clears the current selection.
   */
  @Override
  public void clearSelection() {
    selectionHandler.clearSelection();
  }
}
