/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard;

import minidraw.framework.Figure;
import minidraw.framework.FigureChangeListener;
import minidraw.framework.FigureCollection;
import minidraw.framework.ZOrder;

import java.util.Collections;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/** Standard implementation of container/collection of figures.
 * Obeys normal 'emit events to listeners' for any add/remove/zOrder
 * calls.
 */
public class StandardFigureCollection implements FigureCollection {
  protected FigureChangeListener figureChangeListener;

  /** the underlying list of figures handled */
  protected List<Figure> figureList;

  protected Map<String, Figure> mapTag2Figure;

  /** read/write lock guarding the figure list */
  ReadWriteLock lock = new ReentrantReadWriteLock();
  Lock readLock = lock.readLock();
  Lock writeLock = lock.writeLock();

  /**
   * Create a collection of figures with associated change listener.
   * @param figureChangeListener the listener to any figure change
   *                             events originating from figures within
   *                             this collection
   */
  public StandardFigureCollection(FigureChangeListener figureChangeListener) {
    // For the time being, we use an 'expensive' array implementation which
    // avoid concurrent modification issues during iteration.
    figureList = new CopyOnWriteArrayList<>();
    this.figureChangeListener = figureChangeListener;
    mapTag2Figure = new HashMap<>();
  }

  public Figure add(Figure figure) {
    writeLock.lock();
    try {
      if (!figureList.contains(figure)) {
        figureList.add(figure);
        figure.addFigureChangeListener(figureChangeListener);
        figure.changed();
      }
    } finally {
      writeLock.unlock();
    }
    return figure;
  }

  @Override
  public Figure add(Figure figure, String tagName) {
    writeLock.lock();
    Figure theFigure = null;
    try {
      theFigure = add(figure);
      mapTag2Figure.put(tagName, theFigure);
    } finally {
      writeLock.unlock();
    }
    return theFigure;
  }

  public synchronized Figure remove(Figure figure) {
    writeLock.lock();
    try {
      if (figureList.contains(figure)) {
        figureList.remove(figure);
        figure.changed();
        figure.removeFigureChangeListener(figureChangeListener);
      }
    } finally {
      writeLock.unlock();
    }
    return figure;
  }

  /**
   * Get iterator over list of figures.
   * @return iterator over the figures in the composite
   */
  public Iterator<Figure> iterator() {
    Iterator<Figure> iterator = null;
    readLock.lock();
    try {
      iterator = figureList.iterator();
    } finally { readLock.unlock(); }
    return iterator;
  }

  @Override
  public Figure get(int indexOfFigure) {
    if (indexOfFigure < 0 || indexOfFigure >= figureList.size())
      return null;

    Figure theFigure = null;
    readLock.lock();
    try {
      theFigure = figureList.get(indexOfFigure);
    } finally {
      readLock.unlock();
    }

    return theFigure;
  }

  @Override
  public Figure getByTag(String tagName) {
    Figure theFigure = null;
    readLock.lock();
    try {
      theFigure = mapTag2Figure.get(tagName);
    } finally {
      readLock.unlock();
    }

    return theFigure;
  }

  public Figure findFigure(int x, int y) {
    // Figures are drawn in order 0..size of fFigures,
    // which means that last added figure is on top.
    // Thus to find the "right" figure when they overlap
    // we must iterate backwards (otherwise the user
    // clicks a figure but gets any figure behind it).
    Figure found = null;

    readLock.lock();;
    // doing it the old raw way
    try {
      int i = figureList.size() - 1;
      while (i >= 0) {
        Figure f = figureList.get(i);
        if (f.displayBox().contains(x, y)) {
          found = f;
          break;
        }
        i--;
      }
    } finally { readLock.unlock(); }
    return found;
  }

  @Override
  public Figure zOrder(Figure figure, ZOrder order) {
    // Figures are drawn in the order they appear in
    // figureList, that is element 0 first, then
    // element 1, etc. That is, the bottom element
    // has index 0, the top has size()-1.

    writeLock.lock();
    try {
      int index = figureList.indexOf(figure);
      int lastIndex = figureList.size() - 1;
      switch (order) {
        case TO_BOTTOM: {
          // Remove figure (fires change events) and add at bottom
          figureList.remove(index);
          // Add at bottom of stack
          figureList.add(0, figure);
          figure.changed();
          break;
        }
        case FORWARD: {
          // Do nothing in case index == lastIndex, already in front
          if (index == lastIndex) {
            return figure;
          }
          // Swap current figure to a later position (UP in stack)
          Collections.swap(figureList, index, index + 1);
          // And mark both as changed
          figureList.get(index).changed();
          figureList.get(index + 1).changed();
          break;
        }
        case BACKWARD: {
          // Do nothing in case index == 0, already at Back
          if (index == 0) {
            return figure;
          }
          // Swap current figure to a earlier position (DOWN in stack)
          Collections.swap(figureList, index, index - 1);
          // And mark both as changed
          figureList.get(index).changed();
          figureList.get(index - 1).changed();
          break;
        }
        case TO_TOP: {
          // Do nothing in case index == lastIndex, already at TOP
          if (index == lastIndex) {
            return figure;
          }
          // Swap current figure to a TOP position (TOP in stack)
          figureList.remove(index);
          figureList.add(figure);
          figure.changed();
          break;
        }
      }
    } finally { writeLock.unlock(); }
    return figure;
  }

  @Override
  public Lock readLock() {
    return readLock;
  }

  @Override
  public Lock writeLock() {
    return writeLock;
  }
}
