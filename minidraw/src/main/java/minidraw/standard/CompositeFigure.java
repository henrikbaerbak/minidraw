/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard;

import minidraw.framework.*;

import java.awt.*;
import java.util.Iterator;
import java.util.concurrent.locks.Lock;

/**
 * A Figure consisting of a set of Figures. Essentially implements a
 * Composite Pattern.
 *
 * This allows you to 'build a single figure' which internally is read
 * from multiple ImageFigures or other figures.
 *
 * Introduced in MiniDraw 3.0 as a fully compositional Design.
 * 
 */

public class CompositeFigure extends AbstractFigure
        implements FigureCollection, FigureChangeListener {

  // Reuse the standard behaviour of a figure collection
  private final FigureCollection figureCollection;

  public CompositeFigure() {
    figureCollection = new StandardFigureCollection(this);
  }

  public Figure add(Figure figure) {
    return figureCollection.add(figure);
  }

  @Override
  public Figure add(Figure figure, String tagName) {
    return figureCollection.add(figure, tagName);
  }

  public Figure remove(Figure figure) {
    return figureCollection.remove(figure);
  }

  public Iterator<Figure> iterator() {
    return figureCollection.iterator();
  }

  public Figure findFigure(int x, int y) {
    return figureCollection.findFigure(x, y);
  }

  @Override
  public Figure zOrder(Figure figure, ZOrder order) {
    return figureCollection.zOrder(figure, order);
  }

  @Override
  public Lock readLock() {
    return figureCollection.readLock();
  }

  @Override
  public Lock writeLock() {
    return figureCollection.writeLock();
  }

  @Override
  public void draw(Graphics g) {
    readLock().lock();
    try {
      for (Figure f : figureCollection) {
        f.draw(g);
      }
    } finally {
      readLock().unlock();
    }
  }

  @Override
  protected void basicMoveBy(int dx, int dy) {
    writeLock().lock();
    try {
    for (Figure f : figureCollection) {
      f.moveBy(dx, dy);
    } } finally {
      writeLock().unlock();
    }
  }

  @Override
  public Rectangle displayBox() {
    boolean theFirstRect = true;
    Rectangle dbox = null;
    readLock().lock();
    try {
      for (Figure f : figureCollection) {
        Rectangle r = (Rectangle) f.displayBox().clone();
        if (theFirstRect) {
          dbox = r;
        } else {
          dbox.add(r);
        }
        theFirstRect = false;
      }
    } finally {
      readLock().unlock();
    }
    if (theFirstRect == true)
      dbox = new Rectangle();
    return dbox;
  }

  @Override
  public void figureInvalidated(FigureChangeEvent e) {
    for (FigureChangeListener l : listenerList) {
      l.figureInvalidated(e);
    }
  }

  @Override
  public void figureChanged(FigureChangeEvent e) {
    for (FigureChangeListener l : listenerList) {
      l.figureChanged(e);
    }
  }

  public Figure get(int indexOfSubfigure) {
    return figureCollection.get(indexOfSubfigure);
  }

  @Override
  public Figure getByTag(String tagName) {
    return figureCollection.getByTag(tagName);
  }
}
