/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard.handlers;

//import javax.swing.*;
import java.awt.Rectangle;
import java.util.*;

import minidraw.framework.*;

/**
 * The Subject role of the observer pattern for DrawingChangeListeners.
 * 
 * You can use this default implementation in all objects that must handle
 * DrawingChangeListeners.
 * 
 */

public class StandardDrawingChangeListenerHandler
    implements DrawingChangeListenerHandler {

  /** list over all associated listeners */
  protected List<DrawingChangeListener> fListeners;

  public StandardDrawingChangeListenerHandler() {
    fListeners = new ArrayList<DrawingChangeListener>();
  }

  /**
   * Adds a listener for this drawing.
   * 
   * @param listener
   *          the listener to add
   */
  @Override
  public void addDrawingChangeListener(DrawingChangeListener listener) {
    fListeners.add(listener);
  }

  /**
   * Removes a listener from this drawing.
   * 
   * @param listener
   *          the listener to remove
   */
  @Override
  public void removeDrawingChangeListener(DrawingChangeListener listener) {
    fListeners.remove(listener);
  }

  /**
   * Fire a 'drawingInvalidated' event
   * 
   * @param source
   *          the drawing this event stems from
   * @param dirtyRectangle the rectangle in need of redrawing
   */
  public void fireDrawingInvalidated(Drawing source, Rectangle dirtyRectangle) {
    for (DrawingChangeListener l : fListeners) {
      l.drawingInvalidated(new DrawingChangeEvent(source, dirtyRectangle));
    }
  }

  /**
   * Fire a 'drawingUpdate' event
   * 
   * @param source
   *          the drawing this event stems from
   */
  public void fireDrawingRequestUpdate(Drawing source) {
    for (DrawingChangeListener l : fListeners) {
      l.drawingRequestUpdate(new DrawingChangeEvent(source, null));
    }
  }
}
