/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.*;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

/**
 * ImageManager is a singleton class that acts as a centralized database of all
 * images to be used in a MiniDraw application. It takes care of automatically
 * loading images to be used in the JHotDraw framework and builds a Map that
 * maps from image names (without extension) to image instances. Images in GIF,
 * PNG, and JPG formats are supported.
 * 
 * <p>
 * As the ImageManager is a singleton class you access it using
 * <tt>ImageManager.getSingleton()</tt>. The MinimalApplication creates the
 * ImageManager automatically.
 * <p>
 *   Updated in MiniDraw v 1.10 to use ImageIO which requires Java 8.
 * </p>
 *
 * <p> Updated in MiniDraw</p>
 */

public class ImageManager {
  /* In the original Ant based code, images are put into a root
   * folder named /resource
   */
  private static final String RESOURCE_PATH_ANT = "/resource/";

  /* To support gradle builds, also look for stuff in
   * /src/main/resources/minidraw/ folder of gradle, which maps simply
   * to /minidraw once build using gradle.
   */
  private static final String RESOURCE_PATH_GRADLE = "/minidraw-images";

  // Will be set after construction to one of the two above paths
  private String resourceFolder;
  // Will be set after construction to the full URL of the resource folder
  private URL resourceRootURL;

  private static Component aComponent;

  private Map<String, Image> name2Image;

  public static ImageManager singleton;
  // Default to 'eager image load'
  private static boolean useLazyLoad = false;

  /** Construct the ImageManager. The
   * parameter is legacy as of version 1.10
   * @param c legacy, can be null.
   */
  ImageManager(Component c) {
    aComponent = c;
    registerAllImages();
    singleton = this;
  }

  ImageManager() {
    this(null);
  }

  public Image getImage(String shortName) {
    String[] supportedExtensions
            = new String[] {".png", ".PNG", ".jpg", ".JPG", ".gif", ".GIF"};
    Image img = name2Image.get(shortName);

    // MiniDraw 2.3 extension: If image files are packed WITHIN the jar file
    // we cannot preload images so the cache may be empty. Thus we try to
    // read the images lazy by guessing at supported extensions.
    if (useLazyLoad && img == null) {
      // Ok, the image was not found in cache and we are told to
      // try anyway (lazy load), so
      int tryIndex = 0;
      img = null;
      // Iterate all 6 supported extensions and try to read in the image
      while (img == null && tryIndex < supportedExtensions.length) {
        try {
          img = loadImage(shortName + supportedExtensions[tryIndex]);
          tryIndex++;
        } catch (IOException e) {
          // is very possible as we are iterating through the set of extension
        }
      }
      // Image still not found - then it is NOT THERE - fail fast!
      if (img == null)
        throw new RuntimeException("ImageManager: Lazy load of image named '" + shortName
                + "' failed. Resource URL is " + resourceRootURL + ".");
      // Great - we found the image, so cache the name and image to avoid any
      // future loads
      name2Image.put(shortName, img);
    }

    // If no-lazy load, we just through up our hands in case image not found.
    if (img == null) {
      throw new RuntimeException("ImageManager: No image named '" + shortName
              + "' has been found in " + resourceRootURL + " folder.");
    }
    return img;
  }

  public static ImageManager getSingleton() {
    if (singleton == null) {
      throw new RuntimeException("getSingleton() invoked before ImageManger "
              + "has been instantiated.");
    }
    return singleton;
  }

  /** load all supported image files in resource folder into the image manager. */
  private void registerAllImages() {
    name2Image = new Hashtable<String, Image>();
    findRootFolderForImagesOrFail();

    try {
      String[] imageFileNameArray = getAllImagesInResourceFolder();
      readAllImages(imageFileNameArray);
    } catch (IOException e) {
      throw new RuntimeException("IOException caught during image load: " + e);
    }
  }

  private void readAllImages(String[] imageFileNameArray) throws IOException {
    for (String s : imageFileNameArray) {
      // Load and register the image
      Image img = loadImage(s);
      if (img == null) {
        throw new RuntimeException(
                "ImageManager: Did not find image named " + s
                        + ", was looking in " + resourceRootURL);
      }
      String filenameNoExtension = s.substring(0, s.length() - 4);
      name2Image.put(filenameNoExtension, img);
    }
  }

  private String[] getAllImagesInResourceFolder() {
    // The list of images to load
    String[] imageFileNameArray = new String[] {};

    // We cannot load images that are embedded with our own
    // jar file! So we switch to lazy loading.
    if (resourceRootURL.getProtocol().equals("jar")) {
      useLazyLoad = true;
      return imageFileNameArray;
    }

    // Otherwise we read the directory and read all files into mem
    String resourcePath;
    try {
      resourcePath = URLDecoder.decode(resourceRootURL.getPath(), "UTF-8");
    }
    catch (UnsupportedEncodingException e) {
      return null; // "UTF-8" is valid, so this should never happen
    }
    File dir = new File(resourcePath);

    // create a filter to find .GIF and other supported files.
    FilenameFilter filter = new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        // TODO: duplicated specification of supported extensions
        return name.endsWith(".gif") || name.endsWith(".GIF")
                || name.endsWith(".png") || name.endsWith(".PNG")
                || name.endsWith(".jpg") || name.endsWith(".JPG");
      }
    };
    // Retrieve array of gif file names in resource folder
    imageFileNameArray = dir.list(filter);
    return imageFileNameArray;
  }

  /** Test the two possible resource folders and set
   * internal state accordingly. If neither are found,
   * throw a runtime exceptions.
   */
  private void findRootFolderForImagesOrFail() {
    // Try the two possible paths for loading graphics
    resourceFolder = RESOURCE_PATH_ANT;
    resourceRootURL = getClass().getResource(resourceFolder);
    if (resourceRootURL == null) {
      resourceFolder = RESOURCE_PATH_GRADLE;
      resourceRootURL = getClass().getResource(resourceFolder);
      if (resourceRootURL == null) {
        throw new RuntimeException(
                "ImageManager: Cannot find minidraw images in folder '" + RESOURCE_PATH_ANT + " or "
                        + RESOURCE_PATH_GRADLE + ". Root folder determined to be : "+resourceRootURL);
      }
    }
  }

  private Image loadImage(String filename) throws IOException {
    String fullFilename = resourceFolder + "/" + filename;
    URL url = ImageManager.class.getResource(fullFilename);
    if (url == null) return null;
    Image img = ImageIO.read(url);
    return img;
  }
}
