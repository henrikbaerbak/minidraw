/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard;

import java.awt.event.*;

import minidraw.framework.*;

/**
 * AbstractTool. Provides some rudimentary behavior for other tools to
 * override.
 *
 * Modeled over the schema from JHotDraw.
 *
 */

public abstract class AbstractTool implements Tool {

  protected DrawingEditor editor;
  protected int fAnchorX, fAnchorY;

  /**
   * Abstract base class for all tools.
   * 
   * @param editor
   *          the editor (object server) that this tool is associated with.
   */
  public AbstractTool(DrawingEditor editor) {
    this.editor = editor;
  }

  @Override
  public void mouseDown(MouseEvent e, int x, int y) {
    fAnchorX = x;
    fAnchorY = y;
  }

  @Override
  public void mouseDrag(MouseEvent e, int x, int y) {
  }

  @Override
  public void mouseUp(MouseEvent e, int x, int y) {
  }

  @Override
  public void mouseMove(MouseEvent evt, int x, int y) {
  }

  @Override
  public void keyDown(KeyEvent evt, int key) {
  }

  public DrawingEditor editor() {
    return editor;
  }

}
