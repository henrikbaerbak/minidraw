/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard;

import java.awt.Rectangle;
import java.util.*;

import minidraw.framework.*;

/**
 * Abstract Figure: Base implementation of some Figure behaviour.
 * Responsibilities: A) All observer role "Subject" base functionality is
 * provided.
 *
 * Code partly copied from JHotDraw 5.1.
 *
 */

public abstract class AbstractFigure implements Figure {

  /** the listeners of this figure */
  protected List<FigureChangeListener> listenerList;

  /** Base construction of a figure */
  public AbstractFigure() {
    listenerList = new ArrayList<>();
  }

  @Override
  public void moveBy(int dx, int dy) {
    willChange();
    basicMoveBy(dx, dy);
    changed();
  }

  @Override
  public void moveTo(int x, int y) {
    willChange();
    int currentX = displayBox().x;
    int currentY = displayBox().y;
    basicMoveBy(x - currentX, y - currentY);
    changed();
  }

  /**
   * Informs that a figure is about to change something that affects the
   * contents of its display box.
   */
  protected void willChange() {
    invalidate();
  }

  @Override
  public void changed() {
    invalidate();
    for (FigureChangeListener l : listenerList) {
      FigureChangeEvent e = new FigureChangeEvent(this);
      l.figureChanged(e);
    }
  }

  @Override
  public void invalidate() {
    for (FigureChangeListener l : listenerList) {
      Rectangle r = (Rectangle) displayBox().clone();
      FigureChangeEvent e = new FigureChangeEvent(this, r);
      l.figureInvalidated(e);
    }
  }

  /**
   * This is the hook method to be overridden when a figure moves. Do not call
   * directly, instead call 'moveBy'.
   * 
   * @param dx
   *          the delta to move in x direction
   * @param dy
   *          the delta to move in y direction
   */
  protected abstract void basicMoveBy(int dx, int dy);

  @Override
  public void addFigureChangeListener(FigureChangeListener l) {
    listenerList.add(l);
  }

  @Override
  public void removeFigureChangeListener(FigureChangeListener l) {
    listenerList.remove(l);
  }
}
