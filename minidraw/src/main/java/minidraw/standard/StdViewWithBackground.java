/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw.standard;

import java.awt.*;

import minidraw.framework.DrawingEditor;

/** A drawing view that paints the background with a fixed image */

public class StdViewWithBackground extends StandardDrawingView {

  private static final long serialVersionUID = 7329205003806037431L;
  Image background;

  /**
   * Create a DrawingView that features a graphical image as background for
   * figures.
   * 
   * @param editor
   *          the editor to be associated with
   * 
   * @param backgroundName
   *          name of an image previously loaded by the image manager.
   */
  public StdViewWithBackground(DrawingEditor editor, String backgroundName) {
    super(editor);
    ImageManager im = ImageManager.getSingleton();
    this.background = im.getImage(backgroundName);
  }

  /**
   * Create a DrawingView that features a graphical image as background for
   * figures.
   * 
   * @param editor
   *          the editor to be associated with
   * 
   * @param background
   *          the image to use as background
   */
  public StdViewWithBackground(DrawingEditor editor, Image background) {
    super(editor);
    this.background = background;
  }

  @Override
  public void drawBackground(Graphics g) {
    g.drawImage(background, 0, 0, null);
  }

  @Override
  public Dimension getPreferredSize() {
    return new Dimension(background.getWidth(null), background.getHeight(null));
  }

  @Override
  public Dimension getMinimumSize() {
    return getPreferredSize();
  }

}
