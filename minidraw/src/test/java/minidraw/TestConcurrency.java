/*
 * Copyright (C) 2010 - 2024. Henrik Bærbak Christensen, Aarhus University.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package minidraw;

import minidraw.framework.Drawing;
import minidraw.framework.Figure;
import minidraw.standard.AbstractFigure;
import minidraw.standard.CompositionalDrawing;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

/** Manual tester of concurrency of the figure list.
 *
 */
public class TestConcurrency {
  public static final int THREAD_COUNT = 100;
  public static final int FIGURE_COUNT = 100;

  public static void main(String[] args) throws InterruptedException {

    final CompositionalDrawing drawing = new CompositionalDrawing();

    // create 1000 threads
    ArrayList<MyThread> threads = new ArrayList<>();
    for (int x = 0; x < THREAD_COUNT; x++) {
      threads.add(new MyThread(drawing));
    }

    // start all of the threads
    Iterator i1 = threads.iterator();
    while (i1.hasNext()) {
      MyThread mt = (MyThread) i1.next();
      mt.start();
    }

    // wait for all the threads to finish
    Iterator i2 = threads.iterator();
    while (i2.hasNext()) {
      MyThread mt = (MyThread) i2.next();
      mt.join();
    }

    // Now see how many figures there are
    int count = 0;
    for (Figure f: drawing) count++;

    System.out.println("Count   : " + count);
    System.out.println("Expected: " + THREAD_COUNT * FIGURE_COUNT);
  }
}

// thread that increments the counter 1000 times.
class MyThread extends Thread {
  Drawing drawing;

  MyThread(Drawing drawing){
    this.drawing = drawing;
  }

  public void run() {
    Figure last = null;
    for (int x = 0; x < TestConcurrency.FIGURE_COUNT+1; x++) {
      last = drawing.add(new StupidFigure());
    }
    drawing.remove(last);
  }
}

class StupidFigure extends AbstractFigure {
  private Rectangle rect = new Rectangle(20,20);

  @Override
  public void draw(Graphics g) {
    // N/a
  }

  @Override
  public Rectangle displayBox() {
    return rect;
  }

  @Override
  protected void basicMoveBy(int dx, int dy) {
    rect.setLocation(dx,dy);
  }
}
