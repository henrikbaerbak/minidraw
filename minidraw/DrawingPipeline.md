How MiniDraw does drawing of figures
====================================

MiniDraw is based upon the AWT/Swing rendering pipeline (See [Swing
Painting](https://www.oracle.com/java/technologies/painting.html)),
which is summerized as:

  * All rendering in AWT must be in the `paint(g)` method.  Never call
    it directly, but call `repaint(....)` instead which queues a
    rendering request. Several repaint calls may be merged. For
    complex rendering use the `repaint(dirty-rect)` variant.

  * The `Graphics` parameter contains this dirty-rect in the form of
    the *clip rectangle* which can be queried from within the
    `paint()` method, using `g.getClipBounds()`.

So MiniDraw implementations must obey

   * Invalidation of a part of the drawing space must call
     `repaint(rect)`
   * DrawingView's `paint()` method should inspect the clip rectangle
     and then do incremental redraw, by only request redrawing of
     figure's in its Drawing, whose bounding box intersect the
     clipping rectangle.

'dirty-rect' represents the minimal rectangle that fully enclose those
areas of the display that needs repainting.

Typical redraw scenario
---

Consider moving a figure, f, using its 'moveBy()' method then the
sequence is

    f: moveBy
        invalidate -> l.figureInvalidated(old clip rectangle)
        (actual coordinate updates)
        changed -> l.figureInvalidated(new clip rectangle)
                -> l.figureChanged(figure source)

The 'Drawing' role implementation is *always* added as
FigureChangeListener, so it gets the three events above and reacts by
forwarding them to it's DrawingChangeListeners, ala

    public void figureInvalidated(FigureChangeEvent e) {
      listenerHandler.fireDrawingInvalidated(this, e.getInvalidatedRectangle());
    }
    (similar for figureChanged)
  
The 'DrawingView' role implementation is *always* added as
DrawingChangeListener to the Drawing role's list, so it will also
receive the above change events, and react by requests to the Swing
rendering pipeline

    public void drawingInvalidated(DrawingChangeEvent e) {
      // Just update internal dirty rectangle
      dirtyRectangle.add(e.getInvalidatedRectangle());
    }

    drawingRequestUpdate
      will simply call 'repairDamage()'
    
which again will invoke Swing directly
  
    repaint(dirtyRectangle.x, dirtyRectangle.y,
            dirtyRectangle.width, dirtyRectangle.height);

Swing will queue these requests (potentially merge some) and then call 

    DrawingView::paint(graphics)
    
of which the `drawDrawing()` must iterate all figures in drawing and
only call 'paint' on those within the clip region ala

    if (f.displayBox().intersects(clip))
        f.draw(g);

Concurrency Issues
----

The Java AWT/Swing runs in a separate thread, so there can be the
normal concurrency issues of the collection of figures being a shared
resource. Furthermore, the dirty-rect must be guarded as multiple
writers may update it concurrently.

In Minidraw 2.2 ReentrantReadWriteLock is used for both guarding the
figure collection as well as the dirty rect in drawing view.
